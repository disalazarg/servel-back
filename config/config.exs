# This file is responsible for configuring your umbrella
# and **all applications** and their dependencies with the
# help of Mix.Config.
#
# Note that all applications in your umbrella share the
# same configuration and dependencies, which is why they
# all use the same configuration file. If you want different
# configurations or dependencies per app, it is best to
# move said applications out of the umbrella.
import Config

# Configure Mix tasks and generators
config :servel,
  ecto_repos: [Servel.Repo]

config :servel_web,
  ecto_repos: [Servel.Repo],
  generators: [context_app: :servel]

# Configures the endpoint
config :servel_web, ServelWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "YQKdQU57cL98X2VongfT/+Ty+S/dWeyLZOktbZlDu9ob1tYEbbN/POLu8zec1qvx",
  render_errors: [view: ServelWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: Servel.PubSub,
  live_view: [signing_salt: "5wLqxj9V"],
  origins: "*"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
