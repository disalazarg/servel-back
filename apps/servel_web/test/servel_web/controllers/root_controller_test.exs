defmodule ServelWeb.RootControllerTest do
  use ServelWeb.ConnCase

  test "GET /servel/api", %{conn: conn} do
    conn = get(conn, "/servel/api")
    assert json_response(conn, 200) == %{"service" => "servel-api", "version" => "0.1.0"}
  end
end
