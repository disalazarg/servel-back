defmodule ServelWeb.Router do
  use ServelWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/servel/api" do
    pipe_through :api

    get "/", ServelWeb.RootController, :index

    forward "/graphql", Absinthe.Plug.GraphiQL,
      schema: ServelWeb.Schema,
      interface: :simple
  end
end
