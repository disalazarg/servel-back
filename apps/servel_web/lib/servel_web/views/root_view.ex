defmodule ServelWeb.RootView do
  use ServelWeb, :view

  def render("index.json", %{service: service, version: version}) do
    %{
      service: service,
      version: version
    }
  end
end
