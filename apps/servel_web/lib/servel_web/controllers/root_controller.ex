defmodule ServelWeb.RootController do
  use ServelWeb, :controller

  def index(conn, _params) do
    render(conn, "index.json", %{service: "servel-api", version: "0.1.0"})
  end
end
