defmodule ServelWeb.Schema do
  use Absinthe.Schema
  alias Servel.Store

  object :person do
    field :rut, non_null(:string)
    field :name, non_null(:string)
    field :gender, :string
    field :address, :string
    field :circum, :string
    field :district, :string
    field :province, :string
    field :region, :string
  end

  object :search do
    field :name, non_null(:string)
    field :region, :integer
  end

  query do
    @desc "Get a single person"
    field :get, :person do
      arg :rut, non_null(:string)

      resolve &Store.get/2
    end

    @desc "Find people by name"
    field :find, non_null list_of non_null :person do
      arg :q, non_null(:string)
      arg :page, :integer, default_value: 0

      resolve &Store.find/2
    end
  end
end
