defmodule Servel.Store.Person do
  use Ecto.Schema

  @primary_key {:rut, :string, []}

  schema "people" do
    field :name, :string
    field :gender, :string
    field :address, :string
    field :circum, :string
    field :district, :string, virtual: true
    field :province, :string, virtual: true
    field :region, :string, virtual: true
  end
end
