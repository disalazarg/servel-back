defmodule Servel.Store do
  @moduledoc """
  Context module for accessing the people repository
  """
  import Ecto.Query, only: [from: 2]
  alias Servel.Repo
  alias Servel.Store.Person

  def get(%{rut: rut}, _context) do
    from(p in base(),
      where: p.rut == ^rut)
    |> Repo.one
    |> statify
  end

  def find(%{q: query, page: page}, _context) do
    from(p in base(),
      where: fragment("? @@ plainto_tsquery('spanish', unaccent(?))", p.name_vec, ^query),
      limit: 20,
      offset: 20 * ^page)
      |> Repo.all
      |> statify
  end

  defp base do
    from(n in Person,
      join: d in fragment("district"), on: d.code == n.district,
      join: p in fragment("province"), on: p.code == d.province,
      join: r in fragment("region"), on: r.code == p.region,
      select: %{n | district: d.name, province: p.name, region: r.name})
  end

  defp statify(nil), do: {:error, "no result found"}
  defp statify(resp), do: {:ok, resp}
end
