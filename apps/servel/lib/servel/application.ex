defmodule Servel.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Servel.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: Servel.PubSub}
      # Start a worker by calling: Servel.Worker.start_link(arg)
      # {Servel.Worker, arg}
    ]

    Supervisor.start_link(children, strategy: :one_for_one, name: Servel.Supervisor)
  end
end
