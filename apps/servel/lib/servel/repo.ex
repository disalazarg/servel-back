defmodule Servel.Repo do
  use Ecto.Repo,
    otp_app: :servel,
    adapter: Ecto.Adapters.Postgres
end
