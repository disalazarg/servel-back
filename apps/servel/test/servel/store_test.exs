defmodule Servel.StoreTest do
  use Servel.DataCase

  alias Servel.Store

  describe "get/2" do
    test "can get a person with a valid rut" do
      assert {:ok, user} = Store.get(%{rut: "8.407.686-4"}, %{})
      assert user.rut == "8.407.686-4"
      assert user.name == "Abarca Gonzalez Luis Enrique"
    end

    test "does not fail when a rut is non-existing" do
      assert {:error, _msg} = Store.get(%{rut: "0-0"}, %{})
    end

    test "retrieves location data as well" do
      assert {:ok, user} = Store.get(%{rut: "8.407.686-4"}, %{})
      assert user.district == "Antártica"
      assert user.region   == "Magallanes y de la Antártica Chilena"
    end
  end

  describe "find/2" do
    test "can get a list of people with a given name as query" do
      assert {:ok, list} = Store.find(%{q: "luis", page: 0}, %{})
      refute list == []
      assert Enum.any?(list, fn person -> person.name =~ ~r/Luis/ end)
    end

    test "does not fail for a nonsensical query" do
      assert {:ok, []} == Store.find(%{q: "non-existing", page: 0}, %{})
    end
  end
end
